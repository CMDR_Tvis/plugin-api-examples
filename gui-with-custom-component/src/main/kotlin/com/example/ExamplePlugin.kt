package com.example

import io.github.commandertvis.plugin.gui
import io.github.commandertvis.plugin.gui.GuiLocation
import io.github.commandertvis.plugin.gui.dsl.GUI
import io.github.commandertvis.plugin.gui.dsl.components.GuiComponent
import io.github.commandertvis.plugin.gui.showGui
import io.github.commandertvis.plugin.handle
import org.bukkit.Material
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.plugin.java.JavaPlugin

fun GUI<Unit>.clickableApple(location: GuiLocation) = location.component(ClickableApple())

class ClickableApple : GuiComponent<Unit> {
    override fun apply(gui: GUI<Unit>, location: GuiLocation) {
        gui.apply {
            location.element(item = ItemStack(Material.APPLE)) {
                onClick { _, _ -> gui { location.update { item = ItemStack(Material.GOLDEN_APPLE) } } }
            }
        }
    }
}

class ExamplePlugin : JavaPlugin() {
    private val prototype = gui(rows = 1) {
        title = "GUI with custom component"
        clickableApple(0 on 0)
        clickableApple(0 on 1)
        clickableApple(0 on 2)
    }

    override fun onEnable() = handle<PlayerJoinEvent> { player.showGui(prototype) }
}

package com.example

import io.github.commandertvis.plugin.command
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() {
        command(name = "command-routes") {
            description = "Command with routes"
            "a" routes { default { action(fun CommandSender.(_: String) { sendMessage("Route a") }) } }
            "b" routes { default { action(fun CommandSender.(_: String) { sendMessage("Route b") }) } }
        }
    }
}

package com.example

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.json.JsonConfigurablePlugin
import io.github.commandertvis.plugin.json.adapters.JacksonAdapter
import org.bukkit.event.player.PlayerJoinEvent

data class Settings(var message: String)

class ExamplePlugin : JsonConfigurablePlugin<Settings>(defaultJsonConfig = Settings(message = "default message")) {
    override val adapter get() = JacksonAdapter(jacksonObjectMapper().setDefaultPrettyPrinter(MinimalPrettyPrinter()))
    override fun onEnable() = handle<PlayerJoinEvent> { player.sendMessage(jsonConfig.message) }
}

package com.example

import io.github.commandertvis.plugin.command
import io.github.commandertvis.plugin.command.contexts.DefaultContext
import io.github.commandertvis.plugin.command.contexts.IntContext
import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.command.parseArguments
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() {
        command(name = "command-with-parsing-by-contexts") {
            description = "This command takes 2 integer arguments and sums them."

            default {
                action(@Contexts(DefaultContext::class)
                fun CommandSender.(_: String, args: List<String>) {
                    val (a, b) = args.parseArguments(IntContext, IntContext)

                    if (a !is Int || b !is Int) {
                        sendMessage("Invalid arguments")
                        return@action
                    }

                    sendMessage("The sum is ${a + b}")
                })
            }
        }
    }
}

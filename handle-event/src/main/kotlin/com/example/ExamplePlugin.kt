package com.example

import io.github.commandertvis.plugin.handle
import org.bukkit.event.EventPriority
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() =
        handle<PlayerJoinEvent>(priority = EventPriority.LOWEST) { player.sendMessage("Welcome! :)") }
}

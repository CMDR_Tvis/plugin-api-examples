plugins {
    alias(libs.plugins.kotlin.jvm)
}

allprojects {
    description = "A set of example plugins using Plugin API project."
    group = "com.example"
    version = "1.0.0"

    repositories {
        mavenCentral()
        maven(url = "https://gitlab.com/api/v4/projects/10077943/packages/maven")
        maven(url = "https://hub.spigotmc.org/nexus/content/repositories/snapshots")
        maven(url = "https://oss.sonatype.org/content/groups/public/")
    }
}

subprojects {
    apply(plugin = "kotlin")

    dependencies {
        compileOnly(rootProject.libs.spigot.api)
        compileOnly(rootProject.libs.plugin.api.chat.components)
        compileOnly(rootProject.libs.plugin.api.command)
        compileOnly(rootProject.libs.plugin.api.common)
        compileOnly(rootProject.libs.plugin.api.coroutines)
        compileOnly(rootProject.libs.plugin.api.gui)
    }

    tasks {
        compileKotlin.get().kotlinOptions.jvmTarget = "1.8"
        processResources.get().expand("description" to description, "pluginName" to "Example", "version" to version)
    }
}

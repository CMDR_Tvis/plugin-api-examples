package com.example

import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.replyComponents
import net.md_5.bungee.api.chat.ClickEvent
import net.md_5.bungee.api.chat.HoverEvent
import org.bukkit.ChatColor
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() = handle<PlayerJoinEvent> {
        player.replyComponents {
            text {
                +"This is a link: "
                color(ChatColor.AQUA)
            }

            text {
                +"Visit me"
                underlined()
                bold()
                color(ChatColor.AQUA)
                onClick(ClickEvent.Action.OPEN_URL) { "http://example.com/" }

                onHover(HoverEvent.Action.SHOW_TEXT) {
                    text {
                        +"Please!"
                        color(ChatColor.RED)
                    }
                }
            }
        }
    }
}

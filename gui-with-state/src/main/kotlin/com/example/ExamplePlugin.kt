package com.example

import io.github.commandertvis.plugin.gui.showGui
import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.sessionGui
import org.bukkit.Material
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    data class State(var value: Int)

    private val prototype = sessionGui(rows = 1, defaultSession = State(value = 0)) {
        title = "GUI with state"

        (0 on 0).element(ItemStack(Material.ARROW)) {
            name = "§aIncrease the value"

            onLeftClick {
                gui {
                    session.value++
                    (0 on 1) update { name = this@sessionGui.session.value.toString() }
                }
            }
        }

        (0 on 1).element(ItemStack(Material.APPLE)) { name = this@sessionGui.session.value.toString() }
    }

    override fun onEnable() = handle<PlayerJoinEvent> { player.showGui(prototype) }
}

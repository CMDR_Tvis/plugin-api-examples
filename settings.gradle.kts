enableFeaturePreview("VERSION_CATALOGS")
rootProject.name = "plugin-api-examples"

include(
    "basic-gui",
    "gui-with-state",
    "basic-command",
    "command-with-subcommands",
    "command-with-parsing-by-contexts",
    "clickable-text-components",
    "command-with-parsing-by-data-class",
    "command-with-custom-context",
    "handle-event",
    "command-with-routes",
    "command-with-permission-check",
    "handle-cancellable-event",
    "reply-with-chat-components",
    "gui-with-custom-component",
    "appendscope-example",
    "json-config-with-custom-jackson-mapper",
    "basic-json-config",
    "basic-yaml-json-config",
    "json-config-with-another-adapter",
    "plugin-with-multiple-configs",
    "handle-async"
)

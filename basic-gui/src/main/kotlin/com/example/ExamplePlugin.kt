package com.example

import io.github.commandertvis.plugin.gui
import io.github.commandertvis.plugin.gui.showGui
import io.github.commandertvis.plugin.handle
import org.bukkit.Material
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    private val prototype = gui(rows = 1) {
        title = "Basic GUI"
        (0 on 4).element(ItemStack(Material.APPLE)) { name = "§4Apple" }
    }

    override fun onEnable() = handle<PlayerJoinEvent> { player.showGui(prototype) }
}

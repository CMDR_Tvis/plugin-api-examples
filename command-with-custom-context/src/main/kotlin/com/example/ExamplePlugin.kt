package com.example

import io.github.commandertvis.plugin.command
import io.github.commandertvis.plugin.command.contexts.CommandContext
import io.github.commandertvis.plugin.command.dsl.Contexts
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    data class TwoWords(val word1: String, val word2: String)

    object TwoWordsContext : CommandContext<TwoWords> {
        override val length
            get() = 2

        override fun resolve(strings: Array<String>) = TwoWords(strings[0], strings[1])
    }

    override fun onEnable() {
        command(name = "command-with-custom-context") {
            description = "This command takes 2 words..."

            default {
                action(@Contexts(TwoWordsContext::class)
                fun CommandSender.(_: String, a: TwoWords) {
                    sendMessage("The first word is ${a.word1}, the second one is ${a.word2}")
                })
            }
        }
    }
}

package com.example

import io.github.commandertvis.plugin.command
import io.github.commandertvis.plugin.command.contexts.DefaultContext
import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.list
import org.bukkit.BanList
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() {
        command(name = "command-with-subcommands") {
            description = "The command with 3 subcommands"
            "a" subcommand {
                action(@Contexts(DefaultContext::class)
                fun CommandSender.(_: String, args: List<String>) {
                    sendMessage("This subcommand lists the arguments provided: $args")
                })
            }

            "b" subcommand {
                action(@Contexts(DefaultContext::class)
                fun CommandSender.(_: String, args: List<String>) {
                    sendMessage("This subcommand prints the quantity of provided arguments: ${args.size}")
                })
            }

            "c" subcommand {
                action(fun CommandSender.(_: String) {
                    if (this !is Player) {
                        sendMessage("Not a player")
                        return@action
                    }

                    BanList.Type.NAME.list.addBan(name, "This subcommand bans you", null, "c")
                })
            }


            default { action(fun CommandSender.(_: String) { sendMessage("Available subcommands: a, b, c") }) }
        }
    }
}

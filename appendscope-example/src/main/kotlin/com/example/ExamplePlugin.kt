package com.example

import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.reply.replyAppending
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.plugin.java.JavaPlugin
import kotlin.random.Random

class ExamplePlugin : JavaPlugin() {
    private val random = Random(System.currentTimeMillis())

    override fun onEnable() =
        handle<PlayerJoinEvent> { player.replyAppending { repeat(times = 10) { +random.nextInt().toString() } } }
}

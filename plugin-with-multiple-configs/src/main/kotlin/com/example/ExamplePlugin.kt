package com.example

import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.json.AdditionalJsonConfiguration
import io.github.commandertvis.plugin.json.JsonConfigurablePlugin
import io.github.commandertvis.plugin.json.adapters.JacksonAdapter
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.plugin.java.JavaPlugin

data class AdditionalSettings(var message: String)

data class MainSettings(var message: String)

class AdditionalConfiguration(plugin: JavaPlugin) : AdditionalJsonConfiguration<AdditionalSettings>(
    defaultJsonConfig = AdditionalSettings("default additional message"),
    plugin = plugin,
    name = "Additional"
)

class ExamplePlugin :
    JsonConfigurablePlugin<MainSettings>(defaultJsonConfig = MainSettings(message = "default message")) {
    override val adapter get() = JacksonAdapter.COMMON
    private val additional = AdditionalConfiguration(plugin = this)

    override fun onEnable() = handle<PlayerJoinEvent> {
        player.sendMessage(
            arrayOf(
                jsonConfig.message,
                additional.jsonConfig.message
            )
        )
    }
}

package com.example

import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.json.JsonConfigurablePlugin
import io.github.commandertvis.plugin.json.adapters.JsonAdapter
import io.github.commandertvis.plugin.json.adapters.YamlJacksonAdapter
import org.bukkit.event.player.PlayerJoinEvent

data class Settings(var message: String)

class ExamplePlugin : JsonConfigurablePlugin<Settings>(defaultJsonConfig = Settings(message = "default message")) {
    override val adapter: JsonAdapter
        get() = YamlJacksonAdapter.COMMON

    override fun onEnable() = handle<PlayerJoinEvent> { player.sendMessage(jsonConfig.message) }
}

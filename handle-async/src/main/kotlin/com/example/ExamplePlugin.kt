package com.example

import io.github.commandertvis.plugin.handleAsync
import io.github.commandertvis.plugin.mainDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import org.bukkit.event.player.PlayerBedEnterEvent
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() {
        handleAsync<PlayerBedEnterEvent> {
            delay(1000)
            withContext(mainDispatcher) { player.sendMessage("A second ago you entered your bed.") }
        }
    }
}

package com.example

import io.github.commandertvis.plugin.cancel
import io.github.commandertvis.plugin.handleCancellable
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() =
        handleCancellable<BlockBreakEvent>(ignoreCancelled = true) {
            if (!player.isOp) {
                player.sendMessage("Don't break blocks!")
                cancel()
            }
        }
}

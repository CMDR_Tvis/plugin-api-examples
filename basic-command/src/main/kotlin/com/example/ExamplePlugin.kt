package com.example

import io.github.commandertvis.plugin.command
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() {
        command(name = "basic-command") {
            description = "The basic command."
            default { action(fun CommandSender.(_: String) { sendMessage("Hello.") }) }
        }
    }
}

package com.example

import io.github.commandertvis.plugin.command
import io.github.commandertvis.plugin.command.contexts.IntContext
import io.github.commandertvis.plugin.command.dsl.Contexts
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() {
        command(name = "command-with-parsing-by-data-class") {
            description = "This command takes 2 integer arguments and sums them."

            default {
                action(@Contexts(IntContext::class, IntContext::class)
                fun CommandSender.(_: String, a: Int?, b: Int?) {
                    if (a == null || b == null) {
                        sendMessage("Invalid args")
                        return
                    }

                    sendMessage("The sum is ${a + b}")
                })
            }
        }
    }
}

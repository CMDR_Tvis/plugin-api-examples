package com.example

import io.github.commandertvis.plugin.handle
import io.github.commandertvis.plugin.replyComponents
import org.bukkit.ChatColor
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() = handle<PlayerJoinEvent> {
        player.replyComponents {
            text {
                +"Hello. Is "
                color(ChatColor.AQUA)
            }

            localizedText {
                +"tile.blockDiamond.name"
                bold()
                color(ChatColor.RED)
            }

            text {
                +" your favorite block?"
                color(ChatColor.AQUA)
            }
        }
    }
}

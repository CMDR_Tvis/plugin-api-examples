package com.example

import io.github.commandertvis.plugin.command
import io.github.commandertvis.plugin.register
import org.bukkit.command.CommandSender
import org.bukkit.permissions.Permission
import org.bukkit.permissions.PermissionDefault
import org.bukkit.plugin.java.JavaPlugin

class ExamplePlugin : JavaPlugin() {
    override fun onEnable() {
        val thePermission = Permission("example.permission", PermissionDefault.OP)
        thePermission.register()

        command(name = "command-routes") {
            description = "Command with routes"

            onCommandExecution { sender, _, _ ->
                if (!sender.hasPermission(thePermission))
                    return@onCommandExecution

                sender.sendMessage("No permission.")
                cancel()
            }

            default { action(fun CommandSender.(_: String) { sendMessage("Wow! You've got the permission.") }) }
        }
    }
}
